import * as Colors from "./colors";


export const extraLargeFontSize = 30;
export const largeFontSize = 22;
export const buttonFontSize = 16;
export const baseFontSize = 14;
export const smallFontSize = 12;
export const smallestFontSize = 10;
export const largeHeaderFontSize = 20;
export const headerFontSize = 18;

const base = {
  alignItems: "center",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
};

export const link = {
  color: Colors.green,
  fontWeight: "bold",
};

export const bodyText = {
  color: Colors.baseText,
  fontSize: smallFontSize,
  lineHeight: 19,
};

export const headerText = {
  color: Colors.baseText,
  fontSize: headerFontSize,
  fontWeight: "bold",
};

export const descriptionText = {
  color: Colors.descText,
  fontSize: baseFontSize,
};

export const descTitle = {
  ...base,
  alignItems: "center",
  fontSize: headerFontSize,
  fontWeight: "500", // TODO was semibold, but semibold is invalid value
};

export const screenHeader = {
  ...base,
  color: Colors.baseText,
  fontSize: largeFontSize,
  fontWeight: "bold",
};

export const challengeInfo = {
  ...base,
  alignItems: "center",
  fontSize: buttonFontSize,
  fontWeight: "normal",
  color: Colors.green,
};
export const screenFooter = {
  ...base,
  ...descriptionText,
};

export const sectionHeader = {
  ...base,
  ...headerText,
};

export const count = {
  ...base,
  ...descriptionText,
};

export const easyLevel = {
  color: Colors.easyLevel,
  fontSize: buttonFontSize,
  fontWeight: "bold",
};

export const advancedLevel = {
  color: Colors.advancedLevel,
  fontSize: buttonFontSize,
  fontWeight: "bold",
};

export const hardLevel = {
  color: Colors.hardLevel,
  fontSize: buttonFontSize,
  fontWeight: "bold",
};
