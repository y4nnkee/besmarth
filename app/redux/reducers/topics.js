const initial = {
  refreshing: false,
  topics: {},
  currentTopicId: -1,
  currentTopicName: null,
  currentTopicDescription: ""
};

export default function topics(state = initial, action) {
  switch (action.type) {
    case "RELOADING_TOPICS": {
      return {
        ...state,
        refreshing: true,
        topics: state.topics,
      };
    }
    case "RECEIVED_TOPICS":
      return {
        ...state,
        refreshing: false,
        topics: action.topics
      };
    case "CHANGE_CURRENT_TOPIC":
      return {
        ...state,
        currentTopicId: action.item.id,
        currentTopicName: action.item.topic_name,
        currentTopicDescription: action.item.topic_description,
        currentTopicImage: action.item.image_level1
      };

    default:
      return state;
  }
}
