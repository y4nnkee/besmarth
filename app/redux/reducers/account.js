const initial = {
  loading: false,
  account: {
    pseudonymous: true,
    username: null,
    first_name: null,
    email: null,
  }
};

// ERRORS are managed locally be components

export default function account(state = initial, action) {
  switch (action.type) {
    case 'ACCOUNT_PENDING':
      return {
        ...state,
        loading: true
      };
    case 'ACCOUNT_UPDATE':
      return {
        ...state,
        account: action.account,
        loading: false
      };
    default:
      return state;
  }
}