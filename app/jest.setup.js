import NetInfo from "@react-native-community/netinfo";
import MockDate from "mockdate";
import MockAsyncStorage from "mock-async-storage";
import * as stackTraceParser from 'stacktrace-parser';

MockDate.set("1997-05-09");

jest.mock("./services/phone-utils");
jest.mock("expo-secure-store");
jest.mock("@react-native-community/netinfo");
NetInfo.fetch.mockResolvedValue({isConnected: true, type: "WIFI"});
// preload stacktraceParser so tests dont fail
stackTraceParser.parse("");

// enables testing of react-native Switch. See AcccountForm-test for example
jest.mock("react-native/Libraries/Components/Switch/Switch", () => "Switch");

const mockImpl = new MockAsyncStorage();
jest.mock("react-native/Libraries/Storage/AsyncStorage", () => mockImpl);

// mock Platform.isTesting() to return true instead of false (would break UIManager.configureNext)
jest.mock("react-native/Libraries/Utilities/Platform",
  () => ({
    ...require.requireActual("react-native/Libraries/Utilities/Platform"),
    isTesting: () => true,
  })
);

jest.mock("react-native/Libraries/Animated/src/NativeAnimatedHelper").mock("react-native-gesture-handler", () => {
  const View = require("react-native/Libraries/Components/View/View");
  return {
    State: {},
    PanGestureHandler: View,
    BaseButton: View,
    Directions: {},
  };
});

// Mock debounce so that is doesnt wait the passed time.
jest.mock("debounce", () => (func) => {
  return (...args) => { // cannot be a arrow-function https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Functions/Pfeilfunktionen
    // still deferr function call to simulize asynchronity
    setTimeout(() => func(...args), 0);
  };
});

jest.mock("./services/Navigation");

const mockConsoleMethod = (realConsoleMethod) => {
  const ignoredMessages = [
    "test was not wrapped in act(...)",
    "Calling .setNativeProps() in the test re",
    "AsyncStorage has been extracted from react-native core"
  ];

  return (message, ...args) => {
    const containsIgnoredMessage = ignoredMessages.some((ignoredMessage) => message.includes(ignoredMessage));

    if (!containsIgnoredMessage) {
      console.log(message, ...args);
    }
  };
};

// Suppress console errors and warnings to avoid polluting output in tests.
console.warn = jest.fn(mockConsoleMethod(console.warn));