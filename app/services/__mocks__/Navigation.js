import React from "react";

// Mocking all functions to have no effect but be checkable for test results
export const navigationRef = React.createRef();
export const navigate = jest.fn();
export const goBack = jest.fn();
export const push = jest.fn();
export default {navigate, push, goBack};