import RemoteStorage from "./RemoteStorage";
import LocalStorage from "./LocalStorage";
import {dateToString, getTodayDate, isToday, notNull, parseDate} from "./utils";

const DAILY_TIP_API_PATH = "/tips/daily";
export const DAILY_TIP_LAST_SEEN_CACHE_KEY = "daily-tip-last-seen";

export async function hasUserSeenDailyTip() {
  const lastSeenDate = await LocalStorage.getCacheItem(DAILY_TIP_LAST_SEEN_CACHE_KEY);
  return notNull(lastSeenDate) && isToday(parseDate(lastSeenDate));
}

export async function storeUserHasSeenDailyTip() {
  const str = getTodayDate();
  await LocalStorage.setCacheItem(DAILY_TIP_LAST_SEEN_CACHE_KEY, str);
}

/**
 * Will check if today's tip of the day is already loaded/cached and will otherwise call
 * the backend api for today's tip.
 */
export function fetchDailyTip() {
  return async (dispatch, getState) => {
    /**
     * Puts the tip of the day into the redux store.
     */
    function putIntoStore(dailyTip) {
      dispatch({
        type: "RECEIVE_DAILY_TIP",
        dailyTip: dailyTip
      });
    }

    const storedTip = getState().dailyTip;
    if (notNull(storedTip) && isToday(parseDate(storedTip.date))) {
      // today's tip of the day is already in the redux store. no need to load if.
      return;
    }

    try {
      const response = await RemoteStorage.get(DAILY_TIP_API_PATH);
      const dailyTip = response.data;
      dailyTip.date = dateToString(getTodayDate());
      putIntoStore(dailyTip);
    } catch (e) {
      dispatch({type: "ERROR_DAILY_TIP"});
      throw new Error(e);
    }
  };
}

