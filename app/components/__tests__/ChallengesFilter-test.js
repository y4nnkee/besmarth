import React from "react";
import {createEmptyTestStore, renderForSnapshot, renderForTest} from "../../test/test-utils";
import {ChallengesFilter} from "../ChallengesFilter";
import {fireEvent, waitForElement} from "@testing-library/react-native";

describe("ChallengesFilter", () => {
  it(`renders`, () => {
    const tree = renderForSnapshot(<ChallengesFilter visible="true"/>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`calls passed onFilter function`, async (done) => {
    const mockFn = jest.fn();
    const {getByLabelText} = renderForTest(<ChallengesFilter visible={true}
                                                             onFilter={mockFn}/>, createEmptyTestStore());

    const queryField = await waitForElement(() => getByLabelText("Suchbegriff"));
    fireEvent.changeText(queryField, "testquery");

    setTimeout(() => {
      expect(mockFn).toBeCalled();
      expect(mockFn).toBeCalledWith({
        difficulty: null,
        category: null,
        query: "testquery"
      });
      done();
    }, 2);
  });
});