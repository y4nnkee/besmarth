import React from "react";
import {createEmptyTestStore, createNetworkMock, renderScreenForTest} from "../../test/test-utils";
import {waitForElement} from "@testing-library/react-native";
import FilteredScreen from "../FilteredScreen";

const mock = createNetworkMock();

let challenge_template = {
    id: 1,
    title: "Hier Titel einfügen",
    description: "Irgendeinen Text als Ersatz für die Beschreibung",
    icon: "http://test.local10/challenges/topic/1/",
    image: "http://test.local10/image/url",
    duration: 42,
    color: "#0B85B5",
    difficulty: "EASY",
    periodicity: "DAILY",
    category: "MOBILITY",
    topic: "NeedsToBeSet"
};

function mockSetup() {
    mock.reset();
    //Energie, Daily, Easy
    challenge_template.difficulty = "EASY";
    challenge_template.topic = "Energie";
    let challenge_a = challenge_template;
    challenge_a.id = 1;
    let challenge_b = challenge_template;
    challenge_b.id = 2;
    mock.onGet("/filterchallenges/?&topic=Energie&periodicity=DAILY&difficulty=EASY").reply(200, [
        challenge_a, challenge_b
    ]);
    mock.onGet("/challenges/topic/1/").reply(200, [
        challenge_a, challenge_b
    ]);

    //Transport, Weekly, Advanced
    challenge_template.difficulty = "ADVANCED";
    challenge_template.topic = "Transport";
    challenge_template.periodicity = "WEEKLY";
    let challenge_c = challenge_template;
    challenge_c.id = 3;
    let challenge_d = challenge_template;
    challenge_d.id = 4;
    mock.onGet("/filterchallenges/?&topic=Transport&periodicity=WEEKLY&difficulty=ADVANCED").reply(200, [
        challenge_c, challenge_d
    ]);
    mock.onGet("/challenges/topic/2/").reply(200, [
        challenge_c, challenge_d
    ]);

    //Abfall, Monthly, Hard
    challenge_template.difficulty = "HARD";
    challenge_template.topic = "Abfall";
    challenge_template.periodicity = "MONTHLY";
    let challenge_e = challenge_template;
    challenge_e.id = 5;
    let challenge_f = challenge_template;
    challenge_f.id = 6;
    mock.onGet("/filterchallenges/?&topic=Abfall&periodicity=MONTHLY&difficulty=HARD").reply(200, [
        challenge_e, challenge_f
    ]);
    mock.onGet("/challenges/topic/3/").reply(200, [
        challenge_e, challenge_f
    ]);

    challenge_template.topic = "Konsum";
    let challenge_g = challenge_template;
    challenge_g.id = 7;
    let challenge_h = challenge_template;
    challenge_h.id = 8;
    mock.onGet("/challenges/topic/4/").reply(200, [
        challenge_g, challenge_h
    ]);
}

describe("FilteredScreen", () => {

    it("renders empty list", async () => {
        mock.reset();
        mock.onGet("/challenges/topic/1/").reply(200, []);
        mock.onGet("/challenges/topic/2/").reply(200, []);
        mock.onGet("/challenges/topic/3/").reply(200, []);
        mock.onGet("/challenges/topic/4/").reply(200, []);
        mock.onGet("/filterchallenges/*").reply(404, []);
        mock.onGet("/filterchallenges/*").reply(404, []);
        mock.onGet("/filterchallenges/*").reply(404, []);

        const {getByTestId, asJSON} = renderScreenForTest(FilteredScreen, createEmptyTestStore());
        waitForElement(() => getByTestId("flatlist"));
        waitForElement(() => getByTestId("tags"));
        waitForElement(() => getByTestId("touch-ripple"));
        expect(asJSON()).toMatchSnapshot();
    });

    it("renders with challenges", async () => {
        mockSetup();

        const {getByTestId, asJSON} = renderScreenForTest(FilteredScreen, createEmptyTestStore());
        waitForElement(() => getByTestId("flatlist"));
        waitForElement(() => getByTestId("tags"));
        waitForElement(() => getByTestId("touch-ripple"));
        expect(asJSON()).toMatchSnapshot();
    });
});
