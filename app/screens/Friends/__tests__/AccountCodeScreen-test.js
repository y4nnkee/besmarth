import {createEmptyTestStore, renderScreenForTest} from "../../../test/test-utils";
import AccountCodeScreen from "../AccountCodeScreen";
import {waitForElement} from "@testing-library/react-native";

describe("AccountCodeScreen", () => {
  it(`renders`, async () => {
    const {asJSON} = renderScreenForTest(AccountCodeScreen, createEmptyTestStore());
    expect(asJSON()).toMatchSnapshot();
  });

  it(`back button works`, async () => {
    const {asJSON, getByLabelText} = renderScreenForTest(AccountCodeScreen, createEmptyTestStore());
    const backBtn = await waitForElement(() => getByLabelText('Zurück'));
    // TODO Does not work with the current test library at the moment. There is always a ReferenceError for navigation.
    // fireEvent.press(backBtn);
    // expect(Navigation.goBack.mock.calls.length).toBe(1);
  });
});