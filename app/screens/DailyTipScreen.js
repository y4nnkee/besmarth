import React from "react";
import {Platform, ScrollView, StyleSheet, View} from "react-native";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Button, Card, Headline, Paragraph} from "react-native-paper";
import {storeUserHasSeenDailyTip} from "../services/DailyTipService";
import Navigation from "../services/Navigation";


class DailyTipScreen extends React.Component {

  componentDidMount() {
    this.unsubscribeListener = this.props.navigation.addListener("focus", this.onScreenFocus.bind(this));
  }

  async onScreenFocus() {
    // whenever this screen is opened, mark daily tip as read
    await storeUserHasSeenDailyTip();
  }

  componentWillUnmount() {
    this.unsubscribeListener();
  }

  render() {
    const tip = this.props.dailyTip;

    let image;
    if (tip.image) {
      image = (<Card.Cover source={{uri: tip.image}}/>);
    }

    return (
      <ScrollView style={styles.container}>
        <Headline style={styles.headline} accessibilityLabel="Tipp des Tages">Tipp des Tages</Headline>
        <Card>
          {image}
          <Card.Title title={tip.title}/>
          <Card.Content>
            <Paragraph>{tip.description}</Paragraph>
            <View style={{display: "flex", justifyContent: "center", margin: 20}}>
              <Button mode="contained" onPress={() => Navigation.navigate("HomeTab", "Home")}>Zur Startseite</Button>
            </View>
          </Card.Content>
        </Card>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  headline: {
    padding: 10,
    paddingTop: Platform.OS === "android" ? 35 : 10
  },
  tipTitle: {
    margin: 10,
  },
  tipText: {
    fontSize: 16,
    padding: 10,
    textAlign: "justify"
  }
});

const mapStateToProps = state => {
  return {dailyTip: state.dailyTip};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DailyTipScreen);