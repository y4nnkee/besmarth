import React from "react";

import {Alert, FlatList, Image, ScrollView, StyleSheet, View, KeyboardAvoidingView, Platform} from "react-native";

import {bindActionCreators} from "redux";
import {
    changeSharedStateChallenge,
    createChallengeProgress,
    fetchChallenge,
    fetchChallengeParticipation,
    getNumberOfCompletedChallenges,
    postChallengeRating,
    progressButtonAvailable,
    signUpChallenge,
    unfollowChallenge
} from "../services/Challenges";
import {connect} from "react-redux";
import {notNull} from "services/utils";
import {ActivityIndicator, Button, Card, FAB, Headline, Text, TextInput} from "react-native-paper";
import {showToast} from "services/Toast";
import {ChallengeProgress} from "components/Challenge/ChallengeProgress";
import {ChallengeInfo} from "components/Challenge/ChallengeInfo";
import {Buttons} from "styles/index";
import {BottomSheet} from 'react-native-btr';
import StarRating from 'react-native-star-rating';
import Navigation from "../services/Navigation";
import Toast from 'react-native-toast-message';
import {headerFontSize} from "../styles/typography";
import * as Colors from "../styles/colors";
import {Typography} from "../styles";

class ChallengeDetailScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            ratingVisible: false,
            starCount: 3,
            ratingComment: ""
        };
    }


    componentDidMount() {
        this.reload().then();
    }

    async reload() {
        try {
            let challenge = this.props?.route?.params?.challenge;
            const wholeChallenge = await fetchChallenge(challenge.id);
            if (wholeChallenge != null) {
                challenge = wholeChallenge;
            }
            this.props.navigation.setOptions({
                title: challenge?.title
            });
            this.setState({loading: true, challenge});
            const participation = await fetchChallengeParticipation(challenge);
            if (participation !== null && !participation.mark_deleted) {
                if (participation.duration !== 0) {
                    this.state.challenge.duration = participation.duration;
                }
                if (participation.periodicity !== 'DEFAULT') {
                    this.state.challenge.periodicity = participation.periodicity;
                }
            }
            this.setState({loading: false, challenge, participation});
        } catch (e) {
            console.log(e);
            this.setState({loading: false, error: "Ein Fehler ist aufgetreten"});
        }
    }

    buildLogProgressButton() {
        if (progressButtonAvailable(this.state.participation)) {
            return (
                <FAB
                    style={Buttons.challengeFab}
                    label="Geschafft"
                    mode="contained"
                    accessibilityLabel="Challenge als erledigt markieren"
                    testID="Challenge als erledigt markieren"
                    onPress={this.onLogProgress.bind(this)}/>
            );
        } else {
            return null;
        }
    }

    handleSignUpChallenge() {
        Alert.alert(
            'Challenge anpassen',
            'Möchten Sie die Dauer und Fequenz der Challenge anpassen?',
            [
                {
                    text: 'Ja',
                    onPress: () => Navigation.push("MyChallengesTab", "CustomizeChallenge", {
                        challenge: this.state.challenge,
                        reloadView: () => {
                            this.reload();
                        }
                    })
                },
                {
                    text: 'Nein',
                    onPress: () => {
                        this.onSignUpChallenge()
                    },
                    style: 'cancel'
                }
            ],
            {cancelable: false}
        );
    }

    async onSignUpChallenge() {
        const participation = await signUpChallenge(this.state.challenge, 0, 'DEFAULT');
        this.setState({
            ...this.state,
            participation
        });
        Toast.show({
            text2: "Gratuliere! Du hast dich erfolgreich für " + this.state.challenge.title + " angemeldet",
            position: 'bottom', type: 'success',
        });
    }

    async onLogProgress() {
        this.setState({
            ...this.state,
            participation: await createChallengeProgress(this.state.challenge)
        });

        //a customized challenge has a duration value in the participation table
        if (this.state.participation.duration !== 0) {
            if (this.state.participation.progress.length % this.state.challenge.duration === 0) {
                Toast.show({
                    text2: "Gratuliere! Du hast die Challenge " + this.state.challenge.title + " erfolgreich abgeschlossen.",
                    position: 'bottom', type: 'success',
                });
                this.setState({ratingVisible: true});
                this.reload();
            }
        } else {
            if (this.state.participation.progress.length % this.state.participation.duration === 0) {
                Toast.show({
                    text2: "Gratuliere! Du hast die Challenge " + this.state.challenge.title + " erfolgreich abgeschlossen.",
                    position: 'bottom', type: 'success',
                });
                this.setState({ratingVisible: true});
                this.reload();
            }
        }
    }

    async unfollowChallenge() {
        this.setState({
            ...this.state,
            participation: await unfollowChallenge(this.state.challenge)
        });
        Toast.show({
            text2: "Challenge deabonniert.",
            position: 'bottom', type: 'success',
        });
        this.reload();
    }

    async onShareChallenge() {
        this.setState({
            ...this.state,
            participation: await changeSharedStateChallenge(this.state.challenge, true)
        });
        Toast.show({
            text2: "Deine Freunde können deinen Fortschritt dieser Challenge nun sehen.",
            position: 'bottom', type: 'success',
        });
    }

    async onUnshareChallenge() {
        this.setState({
            ...this.state,
            participation: await changeSharedStateChallenge(this.state.challenge, false)
        });
        Toast.show({
            text2: "Deine Freunde können deinen Fortschritt nicht mehr sehen.",
            position: 'bottom', type: 'success',
        });
    }

    /**
     * Returns true if the user has already signed up for this challenge
     */
    isChallengeAlreadyStarted() {
        return notNull(this.state.participation) && notNull(this.state.participation?.joined_time) && !this.state.participation?.mark_deleted;
    }

    buildSharingButton() {
        if (this.state.participation?.shared) {
            return <Button
                label="Teilen aufheben"
                accessibilityLabel="Teilen aufheben"
                style={styles.buttonSharing}
                mode="outlined"
                onPress={this.onUnshareChallenge.bind(this)}>Teilen aufheben</Button>;
        } else {
            return <Button
                accessibilityLabel="Mit Freunden teilen"
                style={styles.buttonSharing}
                mode="outlined"
                onPress={this.onShareChallenge.bind(this)}>Mit Freunden teilen</Button>;
        }
    }

    render() {

        if (this.state.loading) {
            return <ActivityIndicator size="large"/>;
        }

        if (this.state.error) {
            return <Text>{this.state.error}</Text>;
        }

        const toggleStarRatingView = () => {
            this.setState({ratingVisible: !this.state.ratingVisible})
        };

        let progressOrSignInView;
        if (this.isChallengeAlreadyStarted()) {
            progressOrSignInView = (
                <View>
                    <View style={{ margin: 15}}>
                        <Text style={Typography.headerText}>Dein Fortschritt</Text>
                        <ChallengeProgress participation={this.state.participation} challenge={this.state.challenge}/>
                        {this.buildLogProgressButton()}
                    </View>

                    <Card style={styles.challengeCard}>
                        <Card.Content>
                            {ChallengeProgress.countCompletedProgress(getNumberOfCompletedChallenges(this.state.participation, this.state.challenge))}
                        </Card.Content>
                    </Card>
                    <Card>
                        <Card.Content>
                             <Button
                label="Teilnehmen"
                accessibilityLabel="Challenge deabonnieren"
                testID="Challenge deabonnieren"
                style={styles.button}
                mode="contained"
                onPress={this.unfollowChallenge.bind(this)}>Deabonnieren</Button>
                            {this.buildSharingButton()}
                        </Card.Content>
                    </Card>
                </View>
            );
        } else {
            progressOrSignInView = (
                <View style={styles.card}>

                    <Button
                label="Teilnehmen"
                accessibilityLabel="Challenge anmelden"
                testID="Challenge anmelden"
                style={styles.button}
                mode="contained"
                onPress={this.handleSignUpChallenge.bind(this)}>Teilnehmen</Button>
                    <Card style={styles.challengeCard}>
                        <Card.Content>
                            {ChallengeProgress.countCompletedProgress(getNumberOfCompletedChallenges(this.state.participation, this.state.challenge))}
                        </Card.Content>
                    </Card>

                </View>
            );
        }

        return (
            <View>
                <ScrollView>
                    <View style={styles.container}>
                        <Image source={{uri: this.state.challenge.image}}        style={styles.image} />
                        <View style={styles.titleFrame}>
                            <Text style={styles.challengeTitle}>{this.state.challenge.title}</Text>
                        </View>
                        <ChallengeInfo challenge={this.state.challenge}/>
                        {progressOrSignInView}
                    </View>
                    {this.state.challenge.comments.length > 0 ?  (
                        <View>
                                            <Headline style={{
                        paddingLeft: 10, fontSize: headerFontSize,
                        fontWeight: "500"
                    }}>Kommentare</Headline>
                        <FlatList style={{height: 200, padding: 5}}
                              horizontal={true}
                              nestedScrollEnabled={true}
                              data={this.state.challenge.comments}
                              renderItem={({item}) =>
                                  <View style={{padding: 5}}>
                                      <Card
                                          style={styles.commentCard}>
                                          <Card.Content>
                                              <Text numberOfLines={2} style={{fontSize: 20}}>{item.user}</Text>
                                              <Text style={{fontSize: 10}}>Erstellt
                                                  am: {item.created_time.substring(0, 10)}</Text>
                                              <StarRating
                                                  containerStyle={{width: 85, alignItems: 'center'}}
                                                  disabled={true}
                                                  maxStars={5}
                                                  rating={item.rating}
                                                  fullStarColor={'#dd0'}
                                                  starSize={17}
                                              />
                                              <Text numberOfLines={6}>{item.comment}</Text>
                                          </Card.Content>
                                      </Card>
                                  </View>}
                    /></View>):
                        <View style={{margin: 10}}>
                            <Text style={Typography.headerText}>Keine Kommentare</Text>
                        </View>
                    }
                </ScrollView>
             {//For iOs bottom sheet view
                 Platform.OS === "ios" &&
                <BottomSheet
                    //Pop up filter screen
                    visible={this.state.ratingVisible}
                    //setting the visibility state of the bottom shee
                    onBackButtonPress={toggleStarRatingView}
                    //Toggling the visibility state
                    onBackdropPress={toggleStarRatingView}
                    //Toggling the visibility state
                >
                    {/*Bottom Sheet inner View*/}
                    <KeyboardAvoidingView
                  style={{flex:1}}
                  behavior={"padding"}
                  enabled={true}>
                    <View style={styles.bottomNavigationViewiOs}>
                        <View
                            accessibilityLabel="bottomsheet-view"
                            style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                                margin: 30,

                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    fontSize: 20,
                                    marginTop: 15
                                }}>
                                Gratuliere! Du hast die Challenge "{this.state.challenge.title}" erfolgreich
                                abgeschlossen.
                            </Text>
                            <Text style={styles.labelText}>Bewerte die Challenge!</Text>
                            <StarRating
                                containerStyle={{alignItems: 'center', paddingHorizontal: 20}}
                                disabled={false}
                                maxStars={5}
                                rating={this.state.starCount}
                                selectedStar={(rating) => this.setState({starCount: rating})}
                                fullStarColor={'#dd0'}
                            />
                            <Text style={styles.labelText}>Kommentar</Text>
                            <ScrollView style={{
                                maxHeight: 150,
                            }}>
                                <TextInput
                                    multiline={true}
                                    numberOfLines={5}
                                    value={this.state.ratingComment}
                                    onChangeText={(text) => this.setState({ratingComment: text})}
                                    maxLength={150}
                                >
                                </TextInput>
                            </ScrollView>
                            <Button
                                mode="contained"
                                onPress={() => {
                                    postChallengeRating(this.state.challenge.id, this.state.starCount, this.state.ratingComment);
                                    this.setState({ratingVisible: false})
                                    this.reload();
                                }}
                                testID="rate-button"
                            >Bewerten
                            </Button>
                              <Button
                                style={{marginBottom:20}}
                                mode="outlined"
                                onPress={() => {
                                    this.setState({ratingVisible: false})
                                }}
                                testID="rate-button"
                            >Nein, danke
                            </Button>
                        </View>

                    </View>
                  </KeyboardAvoidingView>
                </BottomSheet>
                    }
                    {//For other OS bottom sheet view
                        Platform.OS !== "ios" &&
                         <BottomSheet
                    //Pop up filter screen
                    visible={this.state.ratingVisible}
                    //setting the visibility state of the bottom shee
                    onBackButtonPress={toggleStarRatingView}
                    //Toggling the visibility state
                    onBackdropPress={toggleStarRatingView}
                    //Toggling the visibility state
                >
                    {/*Bottom Sheet inner View*/}
                       <View style={styles.bottomNavigationView}>
                        <View
                            accessibilityLabel="bottomsheet-view"
                            style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                                margin: 30,
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    fontSize: 20,
                                }}>
                                Gratuliere! Du hast die Challenge "{this.state.challenge.title}" erfolgreich
                                abgeschlossen.
                            </Text>
                            <Text style={styles.labelText}>Bewerte die Challenge!</Text>
                            <StarRating
                                containerStyle={{alignItems: 'center', paddingHorizontal: 20}}
                                disabled={false}
                                maxStars={5}
                                rating={this.state.starCount}
                                selectedStar={(rating) => this.setState({starCount: rating})}
                                fullStarColor={'#dd0'}
                            />
                            <Text style={styles.labelText}>Kommentar</Text>
                            <ScrollView style={{
                                maxHeight: 150,
                            }}>
                                <TextInput
                                    multiline={true}
                                    numberOfLines={5}
                                    value={this.state.ratingComment}
                                    onChangeText={(text) => this.setState({ratingComment: text})}
                                    maxLength={150}
                                >
                                </TextInput>
                            </ScrollView>
                            <Button
                                mode="contained"
                                onPress={() => {
                                    postChallengeRating(this.state.challenge.id, this.state.starCount, this.state.ratingComment);
                                    this.setState({ratingVisible: false})
                                    this.reload();
                                }}
                                testID="rate-button"
                            >Bewerten
                            </Button>
                             <Button
                                mode="outlined"
                                onPress={() => {
                                    this.setState({ratingVisible: false})
                                    this.reload();
                                }}
                                testID="rate-button"
                            >Nein, danke
                            </Button>
                        </View>

                    </View>
                  </BottomSheet>
                    }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRadius: 10,
        overflow: 'hidden',
        backgroundColor: 'white',
        margin: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 1,
            height: 3,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 5,
    },
    button: {
        borderColor: Colors.green,
        borderWidth: 1,
        backgroundColor: Colors.green,
        color: 'white',
        marginHorizontal: 10,
        marginVertical: 10,
    },
    buttonSharing: {
        borderColor: Colors.green,
        borderWidth: 1,
        backgroundColor: 'white',
        marginHorizontal: 10,
        marginVertical: 10,
    },
    challengeCard: {
        borderStyle: "solid",
        borderColor: "white",
        borderWidth: 1,
    },
    bottomNavigationView: {
        backgroundColor: '#fff',
        width: '100%',
        height: 500,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    },
        bottomNavigationViewiOs: {
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    },
    labelText: {
        color: '#333',
        fontSize: 18,
        fontWeight: '500',
        textAlign: 'center',
        //marginBottom: 25,
        // marginTop:5,
    },
    image: {
        height: 200,
        width: '100%',
    },
    titleFrame: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "flex-start",
        paddingHorizontal: 20,
        paddingVertical: 5,
        backgroundColor: Colors.green,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
	        width: 0,
	        height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,

            elevation: 6,
    },
    challengeTitle: {
        fontSize: 22,
        color: 'white',
    },
    detailsContainer: {
        fontSize: 22,
        color: 'white',
        flex: 1,
    },

    commentCard: {
        width: 220,
        height: 180,
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    }
});

const mapStateToProps = state => {
    return {};
};
const mapDispatchToProps = dispatch => bindActionCreators({
    showToast,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChallengeDetailScreen);








