from ..models import ChallengeRating, Challenge


class RatingCalculator:

    @staticmethod
    def update(challenge_id):

        # Check if challenge exists
        updated_challenge = Challenge.objects.filter(id=challenge_id)
        if updated_challenge.count() == 0:
            raise IndexError

        query_set = ChallengeRating.objects.filter(challenge_id=challenge_id)

        rating_sum = 0
        rating_counter = query_set.count()
        for rating in query_set:
            rating_sum += rating.rating

        updated_challenge = Challenge.objects.get(id=challenge_id)

        new_average_rating = round(rating_sum/rating_counter, 1)
        updated_challenge.rating = new_average_rating
        updated_challenge.save()

    @staticmethod
    def getAverage(challenge_id):
        return Challenge.objects.get(challenge_id).rating



