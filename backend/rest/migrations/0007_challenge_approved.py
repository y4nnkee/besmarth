# Generated by Django 2.2.6 on 2020-12-02 15:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0006_achievedreward'),
    ]

    operations = [
        migrations.AddField(
            model_name='challenge',
            name='approved',
            field=models.BooleanField(default=False),
        ),
    ]
