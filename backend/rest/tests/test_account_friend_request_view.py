from django.urls import reverse
from rest_framework import status

from .bne_base import BneBaseTest
from ..models import Account, FriendRequest, Friendship


class TestAccountFriendRequestView(BneBaseTest):

    def setUp(self):
        super().setUp()
        self.incoming_user1 = Account(username="incoming_user1")
        self.incoming_user2 = Account(username="incoming_user2")
        self.outgoing_user3 = Account(username="outgoing_user3")
        self.outgoing_user4 = Account(username="outgoing_user4")
        self.user5 = Account(username="user5")
        self.user6 = Account(username="user6")
        self.supply_objects(self.incoming_user1,
                            self.incoming_user2,
                            self.outgoing_user3,
                            self.outgoing_user4,
                            self.user5,
                            self.user6)

        self.request1 = FriendRequest(sender=self.incoming_user1, receiver=self.test_user)
        self.request2 = FriendRequest(sender=self.incoming_user2, receiver=self.test_user)
        self.request3 = FriendRequest(sender=self.test_user, receiver=self.outgoing_user3)
        self.request4 = FriendRequest(sender=self.test_user, receiver=self.outgoing_user4)
        self.request5 = FriendRequest(sender=self.user5, receiver=self.user6)
        self.friend1 = Friendship(sender=self.test_user, receiver=self.user6)
        self.supply_objects(self.request1,
                            self.request2,
                            self.request3,
                            self.request4,
                            self.request5,
                            self.friend1)

    def assert_contains_userid(self, friend_requests, account_id, contained=True):
        contains = False
        for friend_request in friend_requests:
            if friend_request["sender"]["id"] == account_id or friend_request["receiver"]["id"] == account_id:
                contains = True
        self.assertTrue(contains == contained)

    def assert_contains_account(self, accounts, account_username, contained=True):
        contains = False
        for account in accounts:
            if account["username"] == account_username:
                contains = True
        self.assertTrue(contains == contained)

    def test_read_friend_request_invalid_param(self):
        friend_request_url = reverse('account-friend-request')
        friend_request_url += "?direction=invalid"
        self.get_response(friend_request_url, exp_status_code=status.HTTP_400_BAD_REQUEST)


    def test_read_friend_request_default_param(self):
        friend_request_url = reverse('account-friend-request')
        response = self.get_response(friend_request_url)
        friend_requests = response.data
        self.assert_contains_userid(friend_requests, self.incoming_user1.id)
        self.assert_contains_userid(friend_requests, self.incoming_user2.id)
        self.assert_contains_userid(friend_requests, self.outgoing_user3.id, contained=False)
        self.assert_contains_userid(friend_requests, self.outgoing_user4.id, contained=False)

    def test_read_friend_request_incoming(self):
        friend_request_url = reverse('account-friend-request')
        friend_request_url += "?direction=in"
        response = self.get_response(friend_request_url)
        friend_requests = response.data
        self.assert_contains_userid(friend_requests, self.incoming_user1.id)
        self.assert_contains_userid(friend_requests, self.incoming_user2.id)
        self.assert_contains_userid(friend_requests, self.outgoing_user3.id, contained=False)
        self.assert_contains_userid(friend_requests, self.outgoing_user4.id, contained=False)

    def test_read_friend_request_outgoing(self):
        friend_request_url = reverse('account-friend-request')
        friend_request_url += "?direction=out"
        response = self.get_response(friend_request_url)
        friend_requests = response.data
        self.assert_contains_userid(friend_requests, self.incoming_user1.id, contained=False)
        self.assert_contains_userid(friend_requests, self.incoming_user2.id, contained=False)
        self.assert_contains_userid(friend_requests, self.outgoing_user3.id)
        self.assert_contains_userid(friend_requests, self.outgoing_user4.id)

    def test_create_friend_request(self):
        friend_request_url = reverse('account-friend-request')
        body = {
            "receiver": self.user5.id
        }
        response = self.client.post(friend_request_url, body)
        self.assertEquals(status.HTTP_201_CREATED, response.status_code)
        friend_requests = response.data
        self.assert_contains_userid(friend_requests, self.outgoing_user3.id)
        self.assert_contains_userid(friend_requests, self.outgoing_user4.id)
        self.assert_contains_userid(friend_requests, self.user5.id)

    def test_create_friend_request_id_not_number(self):
        friend_request_url = reverse('account-friend-request')
        body = {
            "receiver": "abc"
        }
        response = self.client.post(friend_request_url, body)
        self.assertEquals(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_friend_request_receiver_not_exists(self):
        friend_request_url = reverse('account-friend-request')
        body = {
            "receiver": 12345
        }
        response = self.client.post(friend_request_url, body)
        self.assertEquals(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_create_friend_request_receiver_self(self):
        friend_request_url = reverse('account-friend-request')
        body = {
            "receiver": self.test_user.id
        }
        response = self.client.post(friend_request_url, body)
        self.assertEquals(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_friend_request_twice(self):
        friend_request_url = reverse('account-friend-request')
        body = {
            "receiver": self.user5.id
        }
        response = self.client.post(friend_request_url, body)
        self.assertEquals(status.HTTP_201_CREATED, response.status_code)
        response = self.client.post(friend_request_url, body)
        self.assertEquals(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_friend_request_already_exists(self):
        friend_request_url = reverse('account-friend-request')
        # test existing friend request
        body = {
            "receiver": self.outgoing_user4.id
        }
        response = self.client.post(friend_request_url, body)
        self.assertEquals(status.HTTP_400_BAD_REQUEST, response.status_code)

        # test existing friendship
        body = {
            "receiver": self.user6.id
        }
        response = self.client.post(friend_request_url, body)
        self.assertEquals(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_accept_friend_request(self):
        friend_request_url = reverse('account-friend-request-edit', args=[self.request5.id])
        response = self.client.put(friend_request_url)
        self.assertEquals(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_accept_friend_request_id_not_exists(self):
        friend_request_url = reverse('account-friend-request-edit', args=[1234])
        response = self.client.put(friend_request_url)
        self.assertEquals(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_accept_friend_request_wrong_ownership(self):
        friend_request_url = reverse('account-friend-request-edit', args=[self.incoming_user1.id])
        response = self.client.put(friend_request_url)
        friends = response.data
        self.assertEquals(status.HTTP_201_CREATED, response.status_code)
        self.assert_contains_account(friends, self.incoming_user1.username)

        # friend request needs to be deleted
        friend_request_exists = FriendRequest.objects.filter(pk=self.request1.id).exists()
        self.assertFalse(friend_request_exists)

    def test_delete_friend_request(self):
        friend_request_url = reverse('account-friend-request-edit', args=[self.user5.id])
        response = self.client.delete(friend_request_url)
        self.assertEquals(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_delete_friend_request_id_not_exists(self):
        friend_request_url = reverse('account-friend-request-edit', args=[1234])
        response = self.client.delete(friend_request_url)
        self.assertEquals(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_delete_friend_request_wrong_ownership(self):
        friend_request_url = reverse('account-friend-request-edit', args=[self.incoming_user1.id])
        response = self.client.delete(friend_request_url)
        friend_requests = response.data
        self.assertEquals(status.HTTP_200_OK, response.status_code)
        self.assert_contains_userid(friend_requests, self.incoming_user1.username, contained=False)

        # friend request needs to be deleted
        friend_request_exists = FriendRequest.objects.filter(pk=self.request1.id).exists()
        self.assertFalse(friend_request_exists)
