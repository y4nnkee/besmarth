import unittest
from datetime import timedelta

from django.urls import reverse
from django.utils import timezone

from django.test import TestCase
from ..models import Challenge, Topic


class TestAddNewCommentView(TestCase):

    def setUp(self):
        super().setUp()

    @unittest.skip("Under Construction")
    def test_New_Comment(self):
        # Valid Data
        data = {
            "comment": "TestKommentar2000",
            "challenge_id": "1",
            "user_id": "1"
        }

        response = self.client.post(reverse('comment-add'), data)
        self.assertEqual(response.data, "New Comment Added")
        self.assertEqual(response.status_code, 201)
"""
        # Challenge_ID Is missing
        data = {
            "comment": "TestKommentar2000",
        }

        response = self.client.post(reverse('comment-add'), data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "Invalid Request")
"""