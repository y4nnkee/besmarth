from .bne_base import BneBaseTest
from ..models import Challenge, Topic


class TestReturnApprovedChallenges(BneBaseTest):

    def setUp(self):
        super().setUp()

        topic, created = Topic.objects.get_or_create(internal_id=1, topic_name='Abfall')
        self.challenge_not_approved = Challenge(title="A very nice challenge", difficulty="EASY", duration=10,
                                                description="Lorem ipsum dolorus", topic=topic,
                                                periodicity="WEEKLY", approved=False)

        self.challenge_approved = Challenge(title="A very nice challenge", difficulty="EASY", duration=10,
                                            description="Lorem ipsum dolorus", topic=topic,
                                            periodicity="WEEKLY", approved=True)
        self.challenge_not_approved.save()
        self.challenge_approved.save()

    def test_secure_page(self):
        response = self.get_response('/challenges/topic/1/')

        self.challenge_not_approved.clean()
        self.assertEquals(len(response.data), 1)

        self.challenge_not_approved.approved = True
        self.challenge_not_approved.save()
        response_2 = self.get_response('/challenges/topic/1/')
        self.assertEquals(len(response_2.data), 2)


