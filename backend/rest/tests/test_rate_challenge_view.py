import unittest

from django.urls import reverse

from .bne_base import BneBaseTest
from ..models import Account, Challenge, ChallengeParticipation, Topic, ChallengeRating


class TestRateChallengeView(BneBaseTest):

    def setUp(self):
        super().setUp()
        self.lukas = Account(username="lukas95")
        self.supply_objects(self.lukas)

        topic, created = Topic.objects.get_or_create(internal_id=1, topic_name='Abfall')

        challenge1 = Challenge(title="A very nice challenge", difficulty="EASY", duration=10,
                               description="Lorem ipsum dolorus", topic=topic,
                               periodicity="WEEKLY")
        challenge2 = Challenge(title="Yet another nice challenge", difficulty="HARD", description="Lorem ipsum",
                               duration=10, topic=topic,
                               periodicity="DAILY")
        challenge3 = Challenge(title="TopChallenge", difficulty="HARD", description="Lorem ipsum",
                               duration=10, topic=topic,
                               periodicity="DAILY")
        self.supply_objects(challenge1, challenge2, challenge3)

        lukas_part_challenge_1 = ChallengeParticipation(user=self.lukas, challenge=challenge1, shared=True,
                                                        mark_deleted=True)
        test_user_part_challenge_1 = ChallengeParticipation(user=self.test_user, challenge=challenge1, shared=False,
                                                            mark_deleted=True)
        test_user_part_challenge_2 = ChallengeParticipation(user=self.test_user, challenge=challenge2, shared=False,
                                                            mark_deleted=False)
        test_user_part_challenge_3 = ChallengeParticipation(user=self.test_user, challenge=challenge3, shared=False,
                                                            mark_deleted=True)
        self.supply_objects(lukas_part_challenge_1, test_user_part_challenge_1, test_user_part_challenge_2,
                            test_user_part_challenge_3)

    @unittest.skip("Under Construction")
    def testvalideRating(self):
        # Test rate challenge first time
        challenge_id = 1
        rating = 3

        response = self.client.post(reverse('challenge-rating', args=(challenge_id, rating)))
        self.assertEqual(response.status_code, 201)

        # Test rate challenge again (should override Rating)
        rating = 4

        response = self.client.post(reverse('challenge-rating', args=(challenge_id, rating)))
        self.assertEqual(response.status_code, 201)
        self.assertEqual(ChallengeRating.objects.all().count(), 1)

        # Test rate second challenge (should create second Rating in DB)
        challenge_id = 3
        rating = 3
        response = self.client.post(reverse('challenge-rating', args=(challenge_id, rating)))
        self.assertEqual(response.status_code, 201)
        self.assertEqual(ChallengeRating.objects.all().count(), 2)

    @unittest.skip("Under Construction")
    def testInvalideRating(self):
        # Test rate challenge first time
        challenge_id = 44  # invalide Challenge ID
        rating = 3

        response = self.client.post(reverse('challenge-rating', args=(challenge_id, rating)))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data, "unknown challenge")

    @unittest.skip("Under Construction")
    def testChallengeratingWithoutFinishing(self):
        # Test not possible to rate challenge without finishing the challenge
        challenge_id = 2
        rating = 3

        response = self.client.post(reverse('challenge-rating', args=(challenge_id, rating)))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, "challenge has to be finished before rating")

    @unittest.skip("Under Construction")
    def testCorrectAverageValue(self):
        # Create first rating of a challenge with first test user
        challenge_id = 1
        rating = 2

        response = self.client.post(reverse('challenge-rating', args=(challenge_id, rating)))
        self.assertEqual(response.status_code, 201)

        # Create second rating of a challenge with second test user
        self.client.force_authenticate(user=self.lukas)

        challenge_id = 1
        rating = 3
        response = self.client.post(reverse('challenge-rating', args=(challenge_id, rating)))
        self.assertEqual(response.status_code, 201)

        # Get the finished challenge which was rated
        response = self.client.get(reverse('filter-challenge') + '?difficulty=EASY')
        self.assertEqual(response.status_code, 200)

        # Test if correct average is saved in specific challenge
        new_rating = float(response.data[0]['rating'])
        self.assertEqual(new_rating, 2.5)
