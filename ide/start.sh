#! /bin/bash

# Starts both, backend and frontend. Hitting CTRL-C once will terminate the frontend
# and put backend process into foreground. Hit CTRL-C again to terminate backend as well.

# Enables job management required fore fg command
# see https://stackoverflow.com/questions/11821378/what-does-bashno-job-control-in-this-shell-mean
set -m
source backend/env/bin/activate
python backend/manage.py runserver 0.0.0.0:8000 & expo start app && fg
